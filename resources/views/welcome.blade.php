<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
    @vite('resources/css/app.css')
</head>

<body>
    <!-- Navbar -->
    <header class="header sticky top-0 bg-white shadow-md flex items-center justify-between px-8 py-02">
        <!-- logo -->
        <h1 class="w-3/12">
            <a href="">
                <img src="/images/logo.png">
            </a>
        </h1>
        <!-- navigation -->
        <nav class="nav font-semibold text-lg">
            <ul class="flex items-center">
                <li class="p-4 border-b-2 border-green-500 border-opacity-0 hover:border-opacity-100 hover:text-green-500 duration-200 cursor-pointer active">
                    <a href="">Home</a>
                </li>
                <li class="p-4 border-b-2 border-green-500 border-opacity-0 hover:border-opacity-100 hover:text-green-500 duration-200 cursor-pointer">
                    <a href="">About</a>
                </li>
                <li class="p-4 border-b-2 border-green-500 border-opacity-0 hover:border-opacity-100 hover:text-green-500 duration-200 cursor-pointer">
                    <a href="">Products</a>
                </li>
                <li class="p-4 border-b-2 border-green-500 border-opacity-0 hover:border-opacity-100 hover:text-green-500 duration-200 cursor-pointer">
                    <a href="">About</a>
                </li>
            </ul>
        </nav>
    </header>
    <!-- hero section -->
    <div class="">
        <img src="/images/hero.jpeg" alt="" class="w-full">
    </div>

    <!-- form section -->
    <form>
        <div class="grid grid-cols-2 sm:grid-col-1 gap-[24px] mx-[24px] my-[24px]">
            <div class="class grid ">
                <label for="">First Name</label>
                <input type="text" class="border-solid border border-[2px]" />
            </div>
            <div class="class grid">
                <label for="">Last Name</label>
                <input type="text" class="border-solid border border-[2px]" />
            </div>
            <div class="class grid">
                <label for="">Email Name</label>
                <input type="email" class="border-solid border border-[2px]" />
            </div>
            <div class="class grid">
                <label for="">Password</label>
                <input type="password" class="border-solid border border-[2px]" />
            </div>
        </div>
    </form>
</body>

</html>